import React, { Component } from 'react';
import './App.css';
import { PointsOfInterest } from './pois';
import axios from 'axios';
import MenuList  from './MenuList';

class App extends Component {

  componentDidMount() {
      this.getPOIs()
  }

  state = {
    POIs: [],
    lat: 40.782864,
    lng: -73.965355,
    zoom: 12,
    map:'',
    infoWindow: '',
    myMarkers: []
  }

  getPOIs = () => {
    const endPoint = 'https://api.foursquare.com/v2/venues/explore?'
    const parameters = {
      client_id: "ENAKQVLBKHWVS2CBL4R2OWDQD2KKXL0DBP2CRWMV2HXABKP1",
      client_secret: "P2ILHJQFMBS2MHACMZU2EKZ1QI3LKNM11Z3HSC4QU1WHPZDI",
      query: 'sights',
      near: 'New York',
      v:'20182507',
      limit:5
    }

    axios.get(endPoint + new URLSearchParams(parameters))
    .then(response => {
      this.setState({POIs:response.data.response.groups[0].items}, this.renderMap())
      //console.log(response.data.response.groups[0].items);
    })
    .catch(error =>{
      console.log("ERROR" + error);
    })
  }

  // Render map and markers for each Point Of Interest
  renderMap = () => {
    loadJS('https://maps.googleapis.com/maps/api/js?key=AIzaSyDTwqpsaMgNr-bxhWowfQ8fTlZ_MPvErhI&callback=initMap')
    window.initMap = this.initMap
  }

  initMap = () => {
    const map = new window.google.maps.Map(document.getElementById('map'), {
      center:  {lat: this.state.lat, lng: this.state.lng},
      zoom: this.state.zoom,
      mapTypeControl: false
    });

    let infowindow = new window.google.maps.InfoWindow()
    let markers = [];
    this.state.POIs.map(poi => {
      //console.log(poi.venue.location.lat);
      let marker = new window.google.maps.Marker({
        map: map,
        position: {lat: poi.venue.location.lat, lng: poi.venue.location.lng},
        title: poi.venue.name
      })
      console.log(marker);
      let contentString = `${poi.venue.name}`

      marker.addListener('click', function(){
        infowindow.setContent(contentString)
        infowindow.open(map,marker)
        toggleBounce(marker)
      })
      markers.push(marker)
    }) //markers

    function toggleBounce(marker) {
      //console.log(marker);
      if (marker.getAnimation() !== null) {
        marker.setAnimation(null);
      } else {
        marker.setAnimation(window.google.maps.Animation.BOUNCE);
      }

    }

    this.setState({ infoWindow: infowindow });
    this.setState({ myMarkers: markers});


  } //init

  render() {
    return (
      <div className="App">
        <div className="container">
          <div className="">
            <h1>New York City Points Of Interests</h1>
          </div>
          <MenuList
            POIs={this.state.POIs}
            map={this.state.map}
            infoWindow={this.state.infoWindow}
            myMarkers={this.state.myMarkers}
          />
        </div>
          <div id="map"></div>
      </div>
    )
  }
}

function loadJS(src) {
    var ref = window.document.getElementsByTagName("script")[0];
    var script = window.document.createElement("script");
    script.src = src;
    script.async = true;
    script.defer = true;
    ref.parentNode.insertBefore(script, ref);
}
export default App;
