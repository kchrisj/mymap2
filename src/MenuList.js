import React, { Component } from 'react';
import './MenuList.css';
import escapeRegExp from 'escape-string-regexp'

class MenuList extends Component {
  state = {
    query: ''
  }

  updateQuery = (newQuery) => {
    // save the new query string in state and pass the string
    this.setState({ query: newQuery });
    //this.props.filterLocations(newQuery);
  }


  render(){
    console.log(this.props);
    //this.props.myMarkers.map(marker => console.log(marker.title))
    const listing = this.props.POIs;
    const { query } = this.state
    //console.log(listing);
    //console.log(this.props.markers);

    //var highlightedIcon = this.props.markerIcon.makeMarkerIcon('FFFF24');

    let showList;
    if (query) {
      const match = new RegExp(escapeRegExp(query), 'i')
      showList = this.props.myMarkers.filter((list) => match.test(list.title))
      console.log(showList);
    } else {
      showList = this.props.myMarkers; //listing;
      console.log(showList);
    }
    //var myMarkers = this.props.myMarkers;
    //console.log(myMarkers)
    // This function will loop through the listings and hide them all.
    //function hideListings() {
      //console.log(myMarkers.length)
    //  for (var i = 0; i < myMarkers.length; i++) {
    //    console.log(myMarkers);
    //    myMarkers.setMap(null);
    //  }
    //}

    return(
        <div className="poiList">
          <label htmlFor="query"><strong>Search: </strong></label>
          <input id="query" type="text" placeholder="Type here..." value={this.state.query}
            onChange={(event) => this.updateQuery(event.target.value)}/>
          <ul>
            {showList.length > 0 && showList.map((markerName) => (
              <li key={markerName.title}>
                  <a id="listItem" onClick={() => console.log(showList.setMap(null))} onKeyUp={() => alert('Bye')} tabIndex="0" role="menuitem">{markerName.title}</a>
              </li>
            ))}
          </ul>
        </div>

    );
  }
}
export default MenuList;
